/*
 * Copyright (c) 2011 Yan-Jie Wang
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *  1. The origin of this software must not be misrepresented; you must not
 *  claim that you wrote the original software. If you use this software
 *  in a product, an acknowledgment in the product documentation would be
 *  appreciated but is not required.
 *
 *  2. Altered source versions must be plainly marked as such, and must not be
 *  misrepresented as being the original software.
 *
 *  3. This notice may not be removed or altered from any source
 *  distribution.
 */

/* 使用 LodePNG 函式庫載入 PNG */
#include <lodepng.h>

#include <stdio.h>
#include <stdlib.h>
#include <GL/freeglut.h>

/* 視窗大小 */
#define width 400
#define height 400

/* 旋轉角度 */
GLfloat xrot;
GLfloat yrot;
GLfloat xspeed;
GLfloat yspeed;

/* 存放材質貼圖 */
GLuint texture[3];

/* 選擇使用的濾鏡 */
GLuint filter;

/*
 * light 燈光狀態
 * lp    "L"鍵按下
 * fp    "F"鍵按下
 */
bool light;
bool lp;
bool fp;

/* 環境光源 */
GLfloat LightAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f };

/* 散射光 */
GLfloat LightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };

/* 光源位置 */
GLfloat LightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

int LoadGLTextures() {
	/* 因為圖檔沒載入，所以先放 false */
	int status = false;

	/* 分別存放 w(高度)、h(寛度)、data(資料) */
	unsigned int w;
	unsigned int h;
	unsigned char *data;

	/* 為了等一下好判斷  data 是否有資料，先把 data 指向 NULL */
	data = NULL;

	/*
	 * 使用LodePNG，從檔案載入PNG
	 * 注：此處並非真正要用使迴圏，而是避免使用邪惡的 goto。
	 */
	while (!LodePNG_decode32_file(&data, &w, &h, "texture.png")) {
		/* 至此，圖檔已載入成功 */

		/*
		 * 檢查貼圖是否符合大小規範：
		 *  1. 長和寛必須是2的倍數
		 *  2. 長和寛必須小於顯卡限制
		 *  3. 長和寛必須大於64
		 */
		GLint texSize;
		glGetIntegerv(GL_MAX_TEXTURE_SIZE, &texSize); /* 取得顯卡最大可用的大小 */
		if (w > texSize || h > texSize || w < 64 || h < 64 || (w % 2) == 1
				|| (h % 2) == 1)
			break;

		/* 建立貼圖材質 */
		glGenTextures(3, &texture[0]);

		/* 使用第一張的貼圖材質 */
		glBindTexture(GL_TEXTURE_2D, texture[0]);

		/* 不使用濾鏡 */
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		/* 把貼圖材質載入顯卡記憶體 */
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA,
				GL_UNSIGNED_BYTE, data);

		/* 使用第二張的貼圖材質 */
		glBindTexture(GL_TEXTURE_2D, texture[1]);

		/* 使用濾鏡 */
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		/* 把貼圖材質載入顯卡記憶體 */
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA,
				GL_UNSIGNED_BYTE, data);

		/* 貼圖成功 */
		status = true;
		break;
	}

	/* 因為不再使用data，釋放記憶體空間 */
	if (data)
		free(data);

	return status;
}

/* callback function : 視窗大小改變  */
void reshape(int w, int h) {
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);

	/* 設立視覺模式 */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat) w / (GLfloat) h, 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/* 初始化 */
int GLinit(void) {

	/* 平滑著色模式 */
	glShadeModel(GL_SMOOTH);

	/* 使用環境光源 */
	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);

	/* 使用散射光源 */
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);

	/* 啓用光源一號，此時還沒有真正啓用光源 */
	glEnable(GL_LIGHT1);

	/* 設定清除畫面所用的顏色 */
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	/* Depth Buffer 設定 */
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	/* 用貼圖功能 */
	glEnable(GL_TEXTURE_2D);

	/* 讀入貼圖 */
	if (!LoadGLTextures())
		/* 貼圖讀入失敗 */
		return false;

	return true;
}

/* callback function : 繪製  */
void display(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	/* 把圖繪在深 5 單位處 */
	glTranslatef(0.0f, 0.0f, -5.0f);

	/* 旋轉 */
	glRotatef(xrot, 1000.0f, 0.0f, 0.0f);
	glRotatef(yrot, 0.0f, 1.0f, 0.0f);

	/* 使用貼圖 */
	glBindTexture(GL_TEXTURE_2D, texture[filter]);

	/* 開始繪製 */
	glBegin(GL_QUADS);

	/* 前 */
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	/* 後 */
	glNormal3f(0.0f, 0.0f, -1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	/* 上 */
	glNormal3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	/* 下 */
	glNormal3f(0.0f, -1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	/* 左 */
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	/* 右 */
	glNormal3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();

	/* 送出繪圖指令到顯卡，並等候顯卡繪製完畢 */
	glFinish();

	/* 切換前後端的 Buffer */
	glutSwapBuffers();
}

/* callback function : 閒置  */
void idle(void) {

	/*
	 * 暫停 1/60 秒
	 *  註：_sleep() 似乎不被推荐，但我不知道有沒有更好的方法。
	 */
	_sleep(1000 / 60);

	/* 改變角度 */
	xrot += xspeed;
	yrot += yspeed;

	/* 強制重新繪製 */
	glutPostRedisplay();
}

/* callback function : 普通鍵盤按鍵 */
void keyboard(unsigned char key, int x, int y) {
	if (key == 'l' || key == 'L') {
		/* "L" 按下，增加光源 */
		if (light) {
			light = false;
			glDisable(GL_LIGHTING);
		} else {
			light = true;
			glEnable(GL_LIGHTING);
		}
	} else if (key == 'f' || key == 'F') {
		/* "F" 按下，改變濾鏡 */
		filter = filter + 1;
		if (filter >= 2)
			filter = 0;
	} else if (key == 'r' || key == 'R') {
		/* "R" 按下，角度還原 */
		xspeed = 0.0f;
		yspeed = 0.0f;
		xrot = 0.0f;
		yrot = 0.0f;
	}
}

/* callback function : 特殊鍵盤按鍵 */
void specialKeys(int key, int x, int y) {

	/* 旋轉速度控制 */
	if (key == GLUT_KEY_UP)
		xspeed = xspeed + 0.1f;
	else if (key == GLUT_KEY_DOWN)
		xspeed = xspeed - 0.1f;
	else if (key == GLUT_KEY_LEFT)
		yspeed = yspeed + 0.1f;
	else if (key == GLUT_KEY_RIGHT)
		yspeed = yspeed - 0.1f;
}

/* 程式進入點 */
int main(int argc, char** argv) {

	/* 初始化 Freeglut 庫 */
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

	/* 設定視窗大小 */
	glutInitWindowSize(width, height);

	/* 設定視窗位置 */
	glutInitWindowPosition(100, 100);

	/* 建立視窗 */
	glutCreateWindow("hello, world");

	/* 初始化 OpenGL */
	if (!GLinit()) {
		/* 如錯誤，就是貼圖無法載入 */
		printf("Texture load failed.\n");
		return 1;
	}

	/* 設定 callback function */
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialUpFunc(specialKeys);
	glutIdleFunc(idle);

	/* 進入迴圈 */
	glutMainLoop();
	return 0;
}
